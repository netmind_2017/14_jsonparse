package clase.jsonparse;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import clase.jsonparse.R;
import clase.jsonparse.model.Contact;

public class SecondActivity extends AppCompatActivity {

    TextView editText_address,editText_email;
    Button btn_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        //get Contact
        Contact c = (Contact)getIntent().getSerializableExtra("contact");

        //FindviewByIds
        editText_address = (TextView) findViewById(R.id.editText_address);
        editText_email = (TextView) findViewById(R.id.editText_email);
        btn_back = (Button) findViewById(R.id.btn_back);

        //SET TEXT
        editText_address.setText(c.getAddress());
        editText_email.setText(c.getEmail());

        //back
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
