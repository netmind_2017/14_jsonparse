package clase.jsonparse;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import clase.jsonparse.model.Contact;
import clase.jsonparse.adapters.ItemAdapter;
import clase.jsonparse.utils.HttpHandler;

public class MainActivity extends AppCompatActivity {

    private String TAG = MainActivity.class.getSimpleName();

    private  final Handler handler= new Handler();
    public  Boolean isConnected;
    public static ListView list;
    public static ListAdapter adapter;
    public static ArrayList<Contact> contactList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //IS CONNECT
        if(isConnected() == false){
            this.startActivity(new Intent(WifiManager.ACTION_PICK_WIFI_NETWORK));
        }

        //findViewByIds
        list = (ListView) findViewById(R.id.list);

        //Get JSON
        new GetContacts(this,MainActivity.this).execute("http://api.androidhive.info/contacts/");

        //ONCLICK
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View view, final int position, long arg) {
                Intent i = new Intent(getApplicationContext(),SecondActivity.class);
                i.putExtra("contact",contactList.get(position));
                startActivity(i);
            }
        });

    }


    public class GetContacts extends AsyncTask<String, Void, String> {

        public Context context;
        public String jsonStr="";
        public Activity atc;

        public GetContacts (Context context,Activity atc) {
            this.context = context;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... arg) {

            if(isConnected()){
                // Making a request to url and getting response
                HttpHandler sh = new HttpHandler();
                jsonStr = sh.makeServiceCall(arg[0]);
            }else{
                String filename="contacts.json";
                StringBuffer stringBuffer = new StringBuffer();

                try {
                    BufferedReader inputReader = new BufferedReader(new InputStreamReader(this.context.openFileInput(filename)));
                    String inputString;

                    while ((inputString = inputReader.readLine()) != null) {
                        stringBuffer.append(inputString);
                    }

                    jsonStr = stringBuffer.toString();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }


            if (ParseJson(jsonStr))
                return "AsyncTask finished";
            else
                return "Error";
        }

        @Override
        protected void onPostExecute(String retString) {
            super.onPostExecute(retString);

            if(isConnected) {
                WriteJsonInLocalStorage(jsonStr);
            }

            adapter = new ItemAdapter(getApplicationContext(),android.R.layout.simple_list_item_1,contactList);
            list.setAdapter(adapter);
            Toast.makeText(getApplicationContext(),"data has been refreshed",Toast.LENGTH_LONG).show();
        }


    }

    public Boolean ParseJson (String data) {
        try {
            JSONObject jsonObj = new JSONObject(data);
            JSONArray contacts = jsonObj.getJSONArray("contacts");

            for (int i = 0; i < contacts.length(); i++) {
                JSONObject c = contacts.getJSONObject(i);
                String id = c.getString("id");
                String name = c.getString("name");
                String email = c.getString("email");
                String address = c.getString("address");
                String gender = c.getString("gender");

                JSONObject phone = c.getJSONObject("phone");
                String mobile = phone.getString("mobile");
                String home = phone.getString("home");
                String office = phone.getString("office");


                contactList.add(new Contact(id, name, email, address, gender, mobile, home, office));
            }

        } catch (final JSONException e) {
            Log.e("Util", "Impossible to download the json file.");
            Toast.makeText(getApplicationContext(),"Impossible to download the json.",Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }

    public boolean isConnected () {

        boolean isWifiConn;
        boolean isMobileConn;

        ConnectivityManager cm = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        isWifiConn = networkInfo.isConnected();
        networkInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        isMobileConn = networkInfo.isConnected();

        Log.d("Utils", "Wifi connected: " + isWifiConn);
        Log.d("Utils", "Mobile connected: " + isMobileConn);

        if (isWifiConn || isMobileConn) {
            this.isConnected = true;
        }else{
            this.isConnected = false;
        }
        return this.isConnected;
    }

    public void WriteJsonInLocalStorage(String data) {

        String filename="contacts.json";

        FileOutputStream fos;

        try {
            fos = this.openFileOutput(filename, Context.MODE_PRIVATE);
            fos.write(data.getBytes());
            fos.close();

            Toast.makeText(this, filename + " saved", Toast.LENGTH_LONG).show();

        } catch (Exception e) {
            Log.d("Main_activity",e.getMessage());
        }

    }

}
